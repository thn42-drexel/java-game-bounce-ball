This is a Java game applet that will be embedded on an HTML page.

The game is designed by me when I started learning Java in my Introduction to Programming class.

The game is designed using Java and edited using Eclipse. 

Nowadays, Java applet gradually becomes outdated and unable to run on some platforms, mostly because of security factors. 

One quick way to check this game out is to download the entire repository and imported in either Netbeans or Eclipse.